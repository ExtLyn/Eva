# 用 Cangjie 编写的表达式解析器

支持浮点，字符串，布尔数据类型，支持变量赋值。

## 浮点计算与赋值
```
x <- 3.0 + 2.0 * (99.0 + 101.0) + 7.5
```
## 引用变量浮点计算
```
1+(1 -1) + x* 30/9
```
这里的x是由前序的表达式赋值或者通过程序设置的变量

## 字符串计算
```
"yes" in str
x = y
```
## 逻辑运算
```
x1 > 5 and "yes" in str
```
## 调用方式
```cangjie

var context = HashMap<String, Operand>()
context.put("x1",Operand(3.3f64))
context.put(str,Operand("of course yes"))
let result = evaluateExpression("x1 < 5 and \"yes\" in str", context)
```
这里计算结果 result 为true.

## console 里面直接运行

```
###################################################
##  Enter an simple expression.                  ##
##  Support Boolean,Float,String Data Type       ##
##  e.g.                                         ##
##  x <- 3.0 + 2.0 * (99.0 + 101.0) + 7.5        ##
##  1+(1 -1) + x* 30/9                           ##
##  x>0 and x<=20                                ##
##  "yes" = y                                    ##
##  "yes" in y                                   ## 
##  Enter exit for quit                          ##
###################################################

lyn> v<-"You can do it"
You can do it
lyn> "can" in v
true
lyn> 5+3
8.00000
lyn> exit
```


