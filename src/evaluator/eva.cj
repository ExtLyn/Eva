package jobs.evaluator

import std.regex.*
import std.collection.ArrayList
import std.collection.HashMap
import std.collection.map
import std.collection.collectArrayList
import std.collection.collectString

// 判断 token 是否为合法的变量名
func isValidVariableName(str: String): Bool {
    let rule = ##"^[_A-Za-z]*[A-Za-z]+[_]*[_A-Za-z0-9]*$"##
    let re = Regex(rule)
    if (let Some(md) <- re.matcher(str).find()) {
        if (str == "and" || str == "or" || str == "in") {
            false
        } else {
            str == md.matchStr()
        }
    } else {
        false
    }
}

// 将输入的字符串分割成 token 数组
func tokenize(input: String): ArrayList<String> {
    let rule = ##"&&|\d+\.?\d*|".*?"|\w+|\|\||!|>=|<=|=|>|<|\(|\)|\+|-|\*|/"##
    let re = Regex(rule)
    if (let Some(matchstr) <- re.matcher(input).findAll()) {
        matchstr |> map {md: MatchData => md.matchStr()} |> collectArrayList
    } else {
        throw ExpressionFormatException()
    }
}

// 判断操作符的优先级
func precedence(op: String): Int8 {
    match (op) {
        case "!" => 4
        case "*" | "/" => 3
        case "+" | "-" => 2
        case ">" | "<" | ">=" | "<=" | "=" | "in" => 1
        case "&&" | "||" | "and" | "or" => 0
        case _ => -1
    }
}

// 验证符号顺序是否正确，
func verif(preOps: Option<String>, token: String) {
    if (let Some(ops) <- preOps) {
        if (ops != "!" && ops != ")" && token != "(") {
            throw ExpressionFormatException("${ops}${token}")
        }
    }
}

// 将中缀表达式转换为后缀表达式（逆波兰表达式）的函数
func toPostfix(tokens: ArrayList<String>): ArrayList<ExpressionItem> {
    // 输出列表，用于存储转换后的后缀表达式
    var output = ArrayList<ExpressionItem>()
    // 运算符栈，用于存储中间的运算符
    var operators = ArrayList<String>()
    // 用于记录上一个操作符（操作符或括号）
    var preOps: Option<String> = None

    // 遍历输入的标记序列（tokens）
    for (token in tokens) {
        // 尝试将当前标记解析为操作数
        let operand = Operand.fromString(token)

        // 如果标记是操作数或变量名
        if (!operand.isNone() || isValidVariableName(token)) {
            if (let Some(opd) <- operand) {
                // 如果是操作数，则添加到输出列表
                output.append(Opd(opd))
            } else if (isValidVariableName(token)) {
                // 如果是变量名，则作为变量对象添加到输出列表
                output.append(Var(Variable(token)))
            }
            // 当前标记不是运算符或括号，重置 preOps
            preOps = None
        }
            // 如果标记是左括号
            else if (token == "(") {
            // 左括号直接压入运算符栈
            operators.append(token)
            // 校验前一个操作符是否合法
            verif(preOps, token)
            // 更新 preOps 为当前的左括号
            preOps = Some(token)
        }
            // 如果标记是右括号
            else if (token == ")") {
            // 不断弹出运算符栈中的运算符，并添加到输出列表，直到遇到左括号
            while (!operators.isEmpty()) {
                let op = operators.remove(operators.size - 1)
                if (op == "(") {
                    break
                }
                output.append(Opr(Oparetors(op)))
            }
            // 校验前一个操作符是否合法
            verif(preOps, token)
            // 更新 preOps 为当前的右括号
            preOps = Some(token)
        }
            // 如果标记是运算符
            else {
            // 处理栈中的运算符，根据优先级弹出并添加到输出列表
            while (!operators.isEmpty()) {
                let op = operators[operators.size - 1]
                // 如果栈顶运算符的优先级大于等于当前运算符，并且不是单目运算符（如"!"）
                if (precedence(op) >= precedence(token) && op != "!") {
                    output.append(Opr(Oparetors(operators.remove(operators.size - 1))))
                } else {
                    break
                }
            }
            // 当前运算符压入运算符栈
            operators.append(token)
            // 校验前一个操作符是否合法
            verif(preOps, token)
            // 更新 preOps 为当前运算符
            preOps = Some(token)
        }
    }

    // 处理剩余的运算符栈，全部弹出并添加到输出列表
    while (!operators.isEmpty()) {
        output.append(Opr(Oparetors(operators.remove(operators.size - 1))))
    }

    // 返回后缀表达式的结果
    output
}

// 推演后缀表达式的函数，返回最终计算结果
func evaluatePostfix(postfix: ArrayList<ExpressionItem>, context: HashMap<String, Operand>): Operand {
    // 栈，用于计算后缀表达式
    var stack = ArrayList<Operand>()

    // 遍历后缀表达式中的每个项
    for (item in postfix) {
        match (item) {
            // 如果当前项是操作数，则直接压入栈
            case Opd(value) => stack.append(value)
            
            // 如果当前项是变量
            case Var(variable) =>
                // 尝试从上下文中获取变量值
                if (let Some(value) <- context.get(variable.name)) {
                    stack.append(value) // 如果变量存在，将其值压入栈
                } else {
                    // 如果变量不存在，则抛出异常
                    throw VariableNotExistException(variable.name)
                }
            
            // 如果当前项是运算符
            case Opr(op) =>
                // 判断运算符是否是单目运算符（例如逻辑非 "!"）
                let result = if (op.toString() != "!") {
                    // 如果是双目运算符，则从栈中弹出两个操作数
                    let right = stack.remove(stack.size - 1) // 右操作数
                    let left = stack.remove(stack.size - 1)  // 左操作数
                    op.compute(left, right) // 计算结果
                } else {
                    // 如果是单目运算符，则只弹出一个操作数并计算结果
                    let val = stack.remove(stack.size - 1)
                    val.not() // 单目运算符的计算，例如取反
                }
                // 将计算结果压入栈
                stack.append(result)
        }
    }

    // 最终栈中应该只剩下一个元素，即表达式的结果
    stack.remove(stack.size - 1)
}


// 演算表达式并返回结果，同时支持将结果存储在上下文中
public func evaluateExpression(input: String, context: HashMap<String, Operand>): Operand {
    // 将输入字符串按 `<-` 分割，处理赋值语句
    let stmt = input.split("<-")
    
    // 如果表达式包含赋值操作
    if (stmt.size == 2) {
        // 提取右侧表达式，进行词法分析，生成标记序列（tokens）
        let tokens = tokenize(stmt[1])
        // 将标记序列转换为后缀表达式
        let postfix = toPostfix(tokens)
        // 评估后缀表达式，获取结果
        let result = evaluatePostfix(postfix, context)
        
        // 如果左侧变量名非空，将结果存储到上下文中
        if (!stmt[0].isEmpty()) {
            context.put(stmt[0].trimLeft(" ").trimRight(" "), result) // 去除变量名前后的空格
        }
        // 返回结果
        result
    } 
    // 如果表达式不包含赋值操作
    else {
        // 直接将输入表达式进行词法分析
        let tokens = tokenize(input)
        // 转换为后缀表达式
        let postfix = toPostfix(tokens)
        // 评估后缀表达式并返回结果
        evaluatePostfix(postfix, context)
    }
}

